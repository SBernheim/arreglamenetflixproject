/*
 * 
 * This program is intended to send a seq of IRcodes.
 * 
 * The sequences should be entered in seq, and retrieved with the Sketch IRReadSeq
 * example:
 *{
 * {0x20DF10EF,9},
 * {0xFFFFFFFF,1}
 *};
 * 
 * Developed by SBernheim 2019 Montevideo
 */

#include <IRremote.h>

int RECV_PIN = 4;

IRrecv irrecv(RECV_PIN);
IRsend irsend;

decode_results results;

typedef struct timaAndCode {
   unsigned long code;
   int delay_before_play;
} timeAndCode;

timeAndCode seq[]={
                          {0x20DF10EF,9},
{0xFFFFFFFF,1}};


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Dispositivo para reconectar TV lg al WiFi para que ande Netflix");
  Serial.println("Regalo de un nieto a su abuela");
  Serial.println("SBernheim octubre 2019");
  Serial.println(sizeof(seq)/sizeof(seq[0]));
  Serial.print("SEND_PIN: ");
  Serial.println(SEND_PIN);
  delay(100);
  for(int i=0; i<sizeof(seq)/sizeof(seq[0]); i++){
    Serial.print("Codigo enviado: ");
    Serial.println(seq[i].code,HEX);
    Serial.print("Esperamos por ");
    Serial.print(seq[i].delay_before_play*100);
    Serial.println("ms");
   //åSerial.println(sizeof(seq[i].code));
    irsend.sendLG(seq[i].code,sizeof(seq[i].code)*8);
    delay(seq[i].delay_before_play*100);
  }

  Serial.println("seq enviada");
}

void loop() {
  // put your main code here, to run repeatedly:

}
