/*
 * Builded from IRrecv example
 * 
 * This program is intended to decode a IRremote seq of codes.
 * And print them in the Serial termial to then be coppied to FixNeftlixforYourGrandmaExe
 * as an 2Darray of IRcodes and timeintervals.
 * 
 * example:
 *{
 * {0x20DF10EF,9},
 * {0xFFFFFFFF,1}
 *};
 * 
 * Developed by SBernheim 2019 Montevideo
 */

#include <IRremote.h>

int RECV_PIN = 4;

IRrecv irrecv(RECV_PIN);

decode_results results;

void setup()
{
  Serial.begin(9600);
  // In case the interrupt driver crashes on setup, give a clue
  // to the user what's going on.
  irrecv.enableIRIn(); // Start the receiver
}
unsigned long counter=0;

void loop() {
  while (!irrecv.decode(&results)) {
    delay(100);
    counter++;
  }
  Serial.print("{0x");
  Serial.print(results.value, HEX);
  Serial.print(',');
  Serial.print(counter);
  Serial.println("},");
  counter=0;
    irrecv.resume(); // Receive the next value
  
}
